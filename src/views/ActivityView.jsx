import Activity from "../components/Activity/Activity"
import { getActivity } from "../api/activity"
import { useState } from 'react';
import Button from 'react-bootstrap/Button';

const ActivityView = () => {
    const [activity, setActivity] = useState(null);
    const [loading, setLoading] = useState(false);
    const [apiError, setApiError] = useState(null);

    const handleActivityClicked = async () => {
        setLoading(true);
        const [error, activityResponse] = await getActivity();
        if (error !== null) {
            setApiError(error);
        }
        if (activityResponse !== null) {
            setActivity(activityResponse);
        }
        setLoading(false);
    }


    return (
        <>
            {activity &&
                <section>
                    <Activity
                        activity={activity}
                    />
                </section>}

            {!loading && <section>
                <Button variant="primary" onClick={handleActivityClicked}> Show Activity </Button>
            </section>}
            {loading && <p>Getting activity...</p>}
            {apiError && <p>{apiError}</p>}
        </>
    )
}

export default ActivityView