import './App.css';
import ActivityView from './views/ActivityView';

function App() {
  return (
    <div className="App">
     <ActivityView />

    </div>
  );
}

export default App;
